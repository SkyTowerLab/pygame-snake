import random
import pygame
from socket import *
from pygame.locals import *
import time
from config import *

class Apple:
    
    def __init__(self, surface) -> None:
        self.surface = surface
        self.size = APPLE_SIZE
        self.image = pygame.Surface((self.size, self.size))
        self.image.fill(RED)
        
        self.x = 40
        self.y = 100
    
    def get_rect(self):
        return pygame.Rect(self.x, self.y, self.size, self.size)
        
    def update(self):
        pass
    
    def update_position(self):
        self.x = random.randint(0, WIDTH - self.size)
        self.y = random.randint(0, HEIGHT - self.size)
    
    def draw(self):
        self.surface.blit(self.image, (self.x, self.y))
    
class Snake:
    def __init__(self, surface) -> None:
        self.surface = surface
        
        self.size = SNAKE_SIZE
        self.block = pygame.Surface((self.size, self.size))
        self.block.fill(GREEN)
        
        self.start_up()
    
    def start_up(self):
        self.length = 5
        self.x = [100]*self.length
        self.y = [100]*self.length
        self.direction = "down"
    
    def move_up(self):
        self.direction = "up"
    def move_down(self):
        self.direction = "down"
    def move_right(self):
        self.direction = "right"
    def move_left(self):
        self.direction = "left"
    
    def get_head_rect(self):
        return pygame.Rect(self.x[0], self.y[0], self.size, self.size)
    def get_body_rects(self):
        return [pygame.Rect(self.x[i], self.y[i], self.size, self.size) for i in range(1, self.length)]
    
    def try_to_eat(self, apple):
        if self.get_head_rect().colliderect(apple.get_rect()):
            self.grow()
            apple.update_position()
    
    def trying_to_eat_self(self, continue_playing=True):
        if self.get_head_rect().collidelist(self.get_body_rects()) != -1:
            print("GAME -- OVER")
            if continue_playing:
                self.start_up()
            
            return True
        return False
            
    
    def grow(self):
        self.length += 1
        self.x.append(self.x[-1])
        self.y.append(self.y[-1])
    
    def update(self):
        self._move()
        self.check_out_of_window()
    
    def _move(self):
        for i in range(self.length-1, 0, -1):
            self.x[i] = self.x[i-1]
            self.y[i] = self.y[i-1]
        
        if self.direction == "up":
            self.y[0] -= self.size
        elif self.direction == "down":
            self.y[0] += self.size
        elif self.direction == "right":
            self.x[0] += self.size
        elif self.direction == "left":
            self.x[0] -= self.size
    
    def check_out_of_window(self):
        if self.x[0] < 0:
            self.x[0] = WIDTH
        elif self.x[0] > WIDTH:
            self.x[0] = 0
        if self.y[0] < 0:
            self.y[0] = HEIGHT
        elif self.y[0] > HEIGHT:
            self.y[0] = 0
    
    def draw(self):
        for i in range(self.length):
            self.surface.blit(self.block, (self.x[i], self.y[i]))
        

class Game:
    def __init__(self) -> None:
        pygame.init()
        self.running = True
        self.surface = pygame.display.set_mode((WIDTH,HEIGHT))
        pygame.display.set_caption("Snake")
        
        self.snake = Snake(self.surface)
        self.apple = Apple(self.surface)
        
    def run(self):
        while self.running:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_UP:
                        self.snake.move_up()
                    if event.key == K_DOWN:
                        self.snake.move_down()
                    if event.key == K_RIGHT:
                        self.snake.move_right()
                    if event.key == K_LEFT:
                        self.snake.move_left()
                if event.type == QUIT:
                    self.running = False
                
            self.surface.fill(BLACK)
            
            self.snake.update()
            self.snake.draw()
            self.snake.try_to_eat(self.apple)
            self.snake.trying_to_eat_self()
            
            self.apple.update()
            self.apple.draw()
            
            pygame.display.flip()
            
            time.sleep(SLEEP_TIME)
            

if __name__ == "__main__":
    game = Game()
    game.run()