# Snake in PyGame!

If you don't have pygame install: </br>
`pip install pygame`

__Run it__ <br/>
`python game.py`

---

### Structure:
Object Oriented Programming was use because it is the only <mark>_way_</mark> to code it well.

Game class has a run() method. This is the main game loop.

pygame.Surface() is use to draw rectangles because it is easy to replace this for an image later on (which is what an image is in pygame).

Apple has an update() that does nothing, intentional for futures code as this function should be use to do any updates/animation to the apple objects. 

config.py is the game settings
